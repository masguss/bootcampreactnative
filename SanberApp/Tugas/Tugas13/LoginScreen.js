import React, {Component} from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';

export class LoginScreen extends Component {
  render() {
    return (
      <View style={{padding: 5}}>
        <View
          style={{
            alignItems: 'center',
          }}>
          <Image
            style={{
              height: 150,
              width: 150,
              tintColor: '#440047',
            }}
            source={require('../asset/cisco.png')}
          />
        </View>
        <View
          style={{
            padding: 10,
            backgroundColor: '#e8e8e8',
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            marginTop: 20,
            height: '100%',
            width: '100%',
          }}>
          <View>
            <Text
              style={{
                fontSize: 30,
                fontWeight: 'bold',
                color: '#440047',
                textAlign: 'center',
              }}>
              Sign In
            </Text>
            <TextInput
              style={{
                marginTop: 30,
                borderWidth: 0.5,
                borderColor: '#440047',
                marginBottom: 10,
                borderRadius: 10,
                backgroundColor: 'white',
              }}
              placeholder="Username"></TextInput>
            <TextInput
              style={{
                borderWidth: 0.5,
                borderColor: '#440047',
                marginBottom: 10,
                borderRadius: 10,
                backgroundColor: 'white',
              }}
              placeholder="Password"></TextInput>
            <Text style={{textAlign: 'right', color: '#440047'}}>
              Lupa Password?
            </Text>
            <TouchableOpacity
              style={{
                marginTop: 30,
                height: 50,
                width: '100%',
                borderRadius: 10,
                justifyContent: 'center',
                backgroundColor: '#440047',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: 'white',
                  fontSize: 15,
                  fontWeight: 'bold',
                }}>
                LOGIN
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginTop: 10,
                height: 50,
                width: '100%',
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#440047',
                justifyContent: 'center',
                backgroundColor: '#fff',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#440047',
                  fontSize: 15,
                  fontWeight: 'bold',
                }}>
                REGISTER
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default LoginScreen;
