// //soal nomor 1

function range(startNum, finishNum) {
  var number = [];

  if (startNum < finishNum) {
    for (; startNum <= finishNum; startNum++) {
      number.push(startNum);
    }
  } else if (startNum > finishNum) {
    for (; startNum >= finishNum; startNum--) {
      number.push(startNum);
    }
  } else {
    console.log("-1");
  }
  return number;
}
console.log(range(10, 5)); // isi nilainya disini untuk melihat hasil

console.log("----------------------------------------------------------------");

// //soal nomor 2

function rangeWithStep(startNum1, finishNum1, step) {
  var number1 = [];

  if (startNum1 < finishNum1) {
    while (startNum1 <= finishNum1) {
      number1.push(startNum1);
      startNum1 += step;
    }
  } else if (startNum1 > finishNum1) {
    while (startNum1 >= finishNum1) {
      number1.push(startNum1);
      startNum1 -= step;
    }
  }
  return number1;
}
console.log(rangeWithStep(50, 10, 2)); // isi nilainya disini untuk melihat hasil

console.log("----------------------------------------------------------------");

// //soal nomor 3

function sum(startNum1, finishNum1, step = 1) {
  var number = 0;
  if (startNum1 < finishNum1) {
    while (startNum1 <= finishNum1) {
      number += startNum1;
      startNum1 += step;
    }
  } else if (startNum1 > finishNum1) {
    while (startNum1 >= finishNum1) {
      number += startNum1;
      startNum1 -= step;
    }
  }
  return number;
}
console.log(sum(15, 10)); // isi nilainya disini untuk melihat hasil

console.log("----------------------------------------------------------------");

//soal nomor 4

var input;
function dataHandling() {
  input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];
  return input;
}

console.log("Nomor id :", dataHandling(input)[0][0]);
console.log("Nama lengkap :", dataHandling(input)[0][1]);
console.log("TTL :", dataHandling(input)[0][2]);
console.log("Hobby :", dataHandling(input)[0][3]);

console.log("----------------------------------------------------------------");

//soal nomor 5

function balikKata(x) {
  kata = "";
  for (i = x.length - 1; i >= 0; i--) {
    kata += x[i];
  }
  return kata;
}

console.log(balikKata("sanbercode "));

 console.log("----------------------------------------------------------------");

//soal ke 6
// tidak selesai............
function dataHandling2() {
  var input = [
    "0001",
    "Roman Alamsyah ",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
  ];

  input.splice(0, 2);

  return;
}
console.log(dataHandling()));

