//soal nomor 1
var ulang1 = 2;
console.log("LOOPING PERTAMA");
while (ulang1 <= 20) {
  console.log(ulang1, "- I Love Coding");
  ulang1 += 2;
}
var ulang2 = 20;
console.log("LOOPING KEDUA");
while (ulang2 >= 2) {
  console.log(ulang2, "- I Will Become a Mobile Developer");
  ulang2 -= 2;
}

console.log("-------------------------------------------");

//soal ke 2

for (a = 1; a <= 20; a++) {
  if (a % 3 == 1) {
    console.log(a, "- Santai");
  } else if (a % 3 == 2) {
    console.log(a, "- Berkualitas");
  } else {
    console.log(a, "- I Love Coding");
  }
}

console.log("-------------------------------------------");

//soal ke 3

for (a = 1; a <= 4; a++) {
  for (b = 1; b <= 8; b++) {
    console.log("#");
  }
}

console.log("-------------------------------------------");

//soal ke 4

for (a = 1; a <= 7; a++) {
  for (b = 1; b <= a; b++) {
    console.log("#");
  }
}

console.log("-------------------------------------------");

//soal ke 5

var c;
for (a = 1; a <= 8; a++) {
  for (b = 1; b <= 8; b++) {
    c++;
    if (c % 2 == 1) {
      console.log(" ");
    } else {
      console.log("#");
    }
  }
  c = c + 1;
}
