var nama = "jane";
var peran = "warewolf";

if (nama == "" && peran == "") {
  console.log("nama harus di isi!");
} else if (nama == "john" && peran == "") {
  console.log("Halo " + nama, ", Pilih peranmu untuk memulai game!!");
} else if (nama != "") {
  console.log("Selamat datang di dunia warewolf, " + nama);
  if (peran == "penyihir") {
    console.log(
      "Halo " + peran + " " + nama,
      ", Kamu dapat melihat siapa yang menjadi warewolf!!"
    );
  } else if (peran == "guard") {
    console.log(
      "Halo " + peran + " " + nama,
      ", Kamu akan membantu melindungi temanmu dari serangan warewolf!!"
    );
  } else if (peran == "warewolf") {
    console.log(
      "Halo " + peran + " " + nama,
      ", Kamu akan memakan mangsa setiap malam!!"
    );
  }
}
console.log("----------------------------------------------------");

var hari = 17;
var bulan = 8;
var tahun = 1992;
console.log("isi variabel hari sesuai dengan tanggal yang ada di kelender)");
console.log("isi variabel Bulan dengan angka antara 1 - 12)");
console.log("isi variabel tahun dengan angka antara 1900 - 2200)");
console.log("");
switch (true) {
  case bulan == 1 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Januari", tahun);
    break;
  }
  case bulan == 2 && hari <= 28 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Februari", tahun);
    break;
  }
  case bulan == 3 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Maret", tahun);
    break;
  }
  case bulan == 4 && hari <= 30 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "April", tahun);
    break;
  }
  case bulan == 5 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Mei", tahun);
    break;
  }
  case bulan == 6 && hari <= 30 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Juni", tahun);
    break;
  }
  case bulan == 7 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Juli", tahun);
    break;
  }
  case bulan == 8 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Agustus", tahun);
    break;
  }
  case bulan == 9 && hari <= 30 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "September", tahun);
    break;
  }
  case bulan == 10 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Oktober", tahun);
    break;
  }
  case bulan == 11 && hari <= 30 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Nopember", tahun);
    break;
  }
  case bulan == 12 && hari <= 31 && tahun >= 1900 && tahun <= 2200: {
    console.log(hari, "Desember", tahun);
    break;
  }
  default: {
    console.log("Hari, Bulan atau Tahun Tidak Sesuai Ketentuan");
  }
}
