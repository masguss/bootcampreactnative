//soal nomor 1

function teriak() {
  var a = "halo Sanbers!!";
  return a;
}
console.log(teriak());

console.log("---------------------------------------");

//soal nomor 2

function kalikan(num1, num2) {
  return num1 * num2;
}
console.log(kalikan(12, 4));

console.log("---------------------------------------");

//soal nomor 3

function introduce(name, age, address, hobby) {
  var gabung =
    "Nama saya " +
    name +
    " Umur saya " +
    age +
    " Alamat saya " +
    address +
    " dan hobi saya adalah " +
    hobby;
  return gabung;
}
console.log(
  introduce("Gusman,", "21 Tahun,", "Jl Perintis, Makassar,", "Renang.")
);
