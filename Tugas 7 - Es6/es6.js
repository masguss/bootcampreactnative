//soal nomor 1

const golden = (goldenFunction = () => {
  console.log("this is golden!!");
});
golden();

console.log("----------------------------------------------------------------");

//soal nomor 2

const newFunction = function literal(firstName, lastName) {
  const person = {
    firstName,
    lastName,
  };
  return {
    fullName: () => {
      console.log(firstName + " " + lastName);
      return;
    },
  };
};

newFunction("Gusman", "Sanbers").fullName();

console.log("----------------------------------------------------------------");

//soal nomor 3

const person = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation } = person;
console.log(firstName, lastName, destination, occupation);

console.log("----------------------------------------------------------------");

//soal nomor 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);

console.log("----------------------------------------------------------------");

//soal nomor 5

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet  consectetur adipiscing elit ${planet} 
do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before);
