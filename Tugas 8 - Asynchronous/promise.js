function readBooksPromise(time, book) {
  console.log(`sy mlai membaca ${book.name}`);
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      let sisaWaktu = time - book.timeSpent;
      if (sisaWaktu >= 0) {
        console.log(
          `Saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`
        );
        resolve(sisaWaktu);
      } else {
        console.log(`sy tdk pnya wakt baca ${book.name}`);
        reject(sisaWaktu);
      }
    }, book.timeSpent);
  });
}
module.exports = readBooksPromise;
