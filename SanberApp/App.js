import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import RestApi from './Tugas/Tugas14/RestApi';

export default function App() {
  return (
    //<Telegram />
    <RestApi />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
