import React, {Component} from 'react';
import {Image, Text, View} from 'react-native';

export class AboutScreen extends Component {
  render() {
    return (
      <View style={{padding: 10}}>
        <Text
          style={{
            color: '#440047',
            marginTop: 30,
            fontSize: 25,
            fontWeight: 'bold',
            textAlign: 'center',
            marginBottom: 10,
          }}>
          Tentang Saya
        </Text>
        <View
          style={{
            alignItems: 'center',
          }}>
          <Image
            style={{
              height: 150,
              width: 150,
              tintColor: '#440047',
            }}
            source={require('../asset/profile-user.png')}
          />
        </View>
        <Text
          style={{
            color: '#440047',
            marginTop: 5,
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: 'center',
          }}>
          masguss
        </Text>
        <Text
          style={{
            color: '#440047',
            fontSize: 15,
            textAlign: 'center',
          }}>
          React Native Developer
        </Text>
        <View
          style={{
            padding: 5,
            width: '100%',
            height: 170,
            borderRadius: 10,
            backgroundColor: '#e5e5e5',
            borderColor: '#440047',
            borderWidth: 0.5,
            marginTop: 10,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: '#440047',
              fontSize: 25,
              textAlign: 'center',
              marginBottom: 20,
            }}>
            Portofolio
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Image
              style={{width: 40, height: 40, marginRight: 100}}
              source={require('../asset/linkedin.png')}
            />
            <Image
              style={{width: 40, height: 40, marginRight: 100}}
              source={require('../asset/git.png')}
            />
            <Image
              style={{width: 40, height: 40}}
              source={require('../asset/github.png')}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text
              style={{
                marginRight: 100,
                color: '#440047',
                fontSize: 10,
                textAlign: 'center',
                marginBottom: 20,
              }}>
              masguss
            </Text>
            <Text
              style={{
                marginRight: 100,
                color: '#440047',
                fontSize: 10,
                textAlign: 'center',
                marginBottom: 20,
              }}>
              masguss
            </Text>
            <Text
              style={{
                color: '#440047',
                fontSize: 10,
                textAlign: 'center',
                marginBottom: 20,
              }}>
              masguss
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            height: 80,
            borderRadius: 10,
            backgroundColor: '#e5e5e5',
            borderColor: '#440047',
            borderWidth: 0.5,
            marginTop: 10,
            justifyContent: 'space-around',
          }}>
          <Image
            style={{width: 40, height: 40, marginTop: 10}}
            source={require('../asset/gmail.png')}
          />
          <Image
            style={{width: 40, height: 40, marginTop: 10}}
            source={require('../asset/whatsapp.png')}
          />
        </View>
      </View>
    );
  }
}

export default AboutScreen;
